//Anastasia Puchkova and Yuri Landin KTbo1-8
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Node
{
public:
	int value;
	bool used = 0;//for deliting negatives without recursion 
	string pos;
	Node* left = NULL, * right = NULL, * parent = NULL;
};

Node* root;

//1.add root
//2.if root already exists find the parent of our new leaf
//3.find out left or right our new leaf is for its parent by its position
void inputTree(fstream& fs)
{
	string position;
	int value;
	while (fs >> position >> value)
	{
		Node* newLeaf = new Node();
		if (root == NULL)
		{
			root = newLeaf;
			root->value = value;
			root->pos = position;
		}
		else
		{
			Node* index = root;
			int level = position.size() - 1;
			int i = 1;
			while (i < level)
			{
				if (index->left->pos[i] == position[i])
				{
					index = index->left;
				}
				else
				{
					if (index->right->pos[i] == position[i])
					{
						index = index->right;
					}
				}
				i++;
			}
			if (position[level] == '0')
			{
				index->left = newLeaf;
				index->left->pos = position;
				index->left->value = value;
				index->left->parent = index;
			}
			else
			{
				index->right = newLeaf;
				index->right->pos = position;
				index->right->value = value;
				index->right->parent = index;
			}
		}
	}
}

//recursion to write children if they exists
void writeChilren(Node* vertex)
{
	if (vertex->left)
	{
		cout << vertex->left->pos << " " << vertex->left->value << endl;
		writeChilren(vertex->left);
	}
	if (vertex->right)
	{
		cout << vertex->right->pos << " " << vertex->right->value << endl;
		writeChilren(vertex->right);
	}
}

//write the root and all its children, grandchildren, grand-grand...
void writeTree()
{
	if (root)
	{
		cout << root->pos << " " << root->value << endl;
		writeChilren(root);
	}
	else
	{
		cout << "Tree is empty" << endl;
	}
}

//looking for leaves while going down 
//deleting them if fits and coming back to its parent
//until all vertexes would be used in function 
//(maybe not once due to we need to check newborn leaves
//after deleting their chilgren/child)
void deleteNegativeUsingRecursion(Node* vertex)
{
	if (vertex->left)
	{
		deleteNegativeUsingRecursion(vertex->left);
	}
	if (vertex->right)
	{
		deleteNegativeUsingRecursion(vertex->right);
	}
	if ((!vertex->left) && (!vertex->right))
	{
		if (vertex->value < 0)
		{
			Node* tmp = vertex->parent;
			if (tmp->left == vertex)
			{
				tmp->left = NULL;
			}
			if (tmp->right == vertex)
			{
				tmp->right = NULL;
			}
			delete vertex;
		}
	}
}

//loking for leaves while checking vertexes
//marking checked vertex by vertex->used (bool)
//"rising" from the leaves 
void deleteNegativeUsually(Node* index)
{
	while (root->used == 0)
	{
		if (index->left)//index has a child so its not a leaf
		{
			if (index->left->used == 0)//that son wasn't checked
			{
				index = index->left;//we'll check it or go to its child later
			}
			else
			{
				if (((index->right) && (index->right->used)) || (index->right == NULL))//both left and right sons have been
					//checked OR right son doesn't exist (when left isn't negative)
				{
					index->used++;//so vertex and its children will stay (shouldn't be deleted)
				}
			}
		}
		if ((index->left == NULL) || (index->left->used))//we've been at left son OR it doesn't exist
		{
			if (index->right)//so watch the right one
			{
				if (index->right->used == 0)//that son wasn't checked
				{
					index = index->right;//we'll check it or go to its child later
				}
				else
				{
					index->used++;//vertex and its children will stay (shouldn't be deleted)
					index = root;//go back to the root to go to another its son or stop function
				}
			}
		}
		if ((!index->left) && (!index->right) && (!index->used))//vertex is childfree and has not been visited yet
		{
			if (index->value < 0)//negative
			{
				Node* tmp = index->parent;
				if (tmp->left == index)//we need to delete connection with parent
				{
					tmp->left = NULL;
				}
				if (tmp->right == index)
				{
					tmp->right = NULL;
				}
				delete index;//delete leaf
				index = root;//go back to the begining
				tmp->used = 0;//maybe now the parent of deleted leaf became a leaf so we need to check it again
			}
			else
			{
				index->used++;//leaf survives
				index = root;//start from the the begining
			}
		}
		else
		{
			if (index->used)//we have checked vertex and all children so go back to the root
			{
				index = root;
			}
		}
	}
}

int main()
{
	fstream fs;
	fs.open("Tree1.txt", fstream::in | fstream::out);
	cout << "CHoose command:" << endl << "1. REad tree" << endl;
	cout << "2. Delete negative leaves using recursion" << endl;
	cout << "3. Delete negative leaves without recursion" << endl;
	cout << "4. Write tree" << endl << "0. Cancel" << endl;
	int com = 1;
	while (com)
	{
		cout << "Your choice: ";
		cin >> com;
		cout << endl;
		switch (com)
		{
		case (1):
			inputTree(fs);
			break;
		case (2):
			if (root)
			{
				deleteNegativeUsingRecursion(root);
			}
			else
			{
				cout << "No inputed tree" << endl;
			}
			break;
		case (3):
			if (root)
			{
				deleteNegativeUsually(root);
			}
			else
			{
				cout << "No inputed tree" << endl;
			}
			break;
		case (4):
			if (root)
			{
				writeTree();
			}
			else
			{
				cout << "No inputed tree" << endl;
			}
			break;
		case (0):
			break;
		}
	}
	return 0;
}